#!/opt/anaconda/bin/python

import unittest
from cookiecutter.main import cookiecutter
import os
import json
import nbformat as nbf
import subprocess
import shutil
import py_compile

def launch_process(args):
 
    process = subprocess.Popen(args,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT)
   

    stdout = process.communicate()[0]

    if process.returncode != 0 :

        message='Error executing {} ({})'.format(' '.join(args), process.returncode)
    
    else: 
    
        message='Execution completed ({})'.format(process.returncode)

    return message, stdout

class RTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        with open('artifacts/r.json') as json_file:
            self.data = json.load(json_file)
        
        self.app_path = cookiecutter(template='../../cookiecutter-ewf-notebook-stagein-one-to-one', 
             extra_context=self.data, 
             no_input=True)
        
    def test_env_python(self):
        
        outcome = os.path.exists('/opt/anaconda/envs/{}/bin/python'.format(str(self.data['environment'])))
        self.assertEqual(outcome, True)
      
    def test_language(self):
        
        nb = nbf.read(os.path.join(self.app_path, 'src', 'main', 'app-resources', 'notebook', 'libexec', 'input.ipynb'), 4)
    
        self.assertEqual(str(nb['metadata']['kernelspec']['language']), 'R')
    
    def test_compile(self):
        try:
            py_compile.compile(os.path.join(self.app_path, 'src', 'main', 'app-resources', 'notebook', 'run'), doraise=True)
        except:
            self.fail('failed to compile src/main/app-resources/notebook/run')
 
    def test_env_yml(self):
        
        outcome = os.path.exists(os.path.join(self.app_path, 'src', 'main', 'app-resources', 'dependencies', 'R', 'environment.yml'))
        self.assertEqual(outcome, True)

#    @classmethod
#    def tearDownClass(self):
        
#        command = 'sudo conda remove --name {} --all --yes'.format(str(self.data['environment']))
#        launch_process(command.split(' '))

#        shutil.rmtree(self.app_path)

if __name__ == '__main__':
    unittest.main()
