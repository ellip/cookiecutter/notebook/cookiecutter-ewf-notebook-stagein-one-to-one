#!/opt/anaconda/envs/{{cookiecutter.environment}}/bin/python

# Python version {{cookiecutter.python}}

import sys
{% if cookiecutter.python == "2.7" %}
reload(sys)
sys.setdefaultencoding('utf8')
{% endif %}
import os
import io
import shutil
import atexit
from nbconvert.preprocessors import ExecutePreprocessor, CellExecutionError
import nbformat as nbf
import uuid
import ast
import rpy2
import rpy2.robjects as robjects
from rpy2.robjects import r, pandas2ri

import cioppy
ciop = cioppy.Cioppy()

# define the exit codes
SUCCESS = 0
ERR_NB_RUNTIME=10

references = []
identifiers = []
enclosures = []
local_files = [] 

# add a trap to exit gracefully
def clean_exit(exit_code):
    log_level = 'INFO'
    if exit_code != SUCCESS:
        log_level = 'ERROR'  
   
    msg = {SUCCESS: 'Processing successfully concluded',
           ERR_NB_RUNTIME: 'Failed to run notebook'
    }
 
    ciop.log(log_level, msg[exit_code])  

def parametrize(identifier,reference):
    
    global nb
    
    if str(nb['metadata']['kernelspec']['language']) == 'R': # R
        pandas2ri.activate()
    
        for index, cell in enumerate(nb['cells']):
            if str(cell['cell_type']) == 'code':

                if 'data_path <- ' in str(cell['source']):  
                    ciop.log('INFO', 'cell {} updated with \'data_path\' value {}'.format(index, tmp_dir))
                    cell['source'] =  'data_path <- \'{}\''.format(tmp_dir)

                if 'input_identifier <- ' in str(cell['source']):
                    ciop.log('INFO', 'cell {} updated with \'input_identifier\' value {}'.format(index, tmp_dir))
                    cell['source'] =  'input_identifier <- \'{}\''.format(identifier)

                if 'input_reference <- ' in str(cell['source']):  
                    ciop.log('INFO', 'cell {} updated with \'input_reference\' value {}'.format(index, tmp_dir))
                    cell['source'] =  'input_reference <- \'{}\''.format(reference)   

                if '<- setNames(' in str(cell['source']):

                        r_obj = robjects.r(str(cell['source']))

                        if 'title' in list(r_obj.columns) and 'abstract' in list(r_obj.columns) and 'id' in list(r_obj.columns) and 'value' in list(r_obj.columns):
                            ciop.log('INFO', 'cell {} updated with parameter id {} with value {}'.format(index, 
                                                                                                         r_obj.iloc[0].id, 
                                                                                                         ciop.getparam(r_obj.iloc[0].id)))   
                            
                            r_obj.iloc[0].value = ciop.getparam(r_obj.iloc[0].id)
                            
                            cell['source'] = '''{} <- setNames(data.frame(rbind(c({}))), c({}))'''.format(cell['source'].split('<-')[0], 
                                                                                                               ','.join(['"{}"'.format(x) for x in r_obj.values[0]]), 
                                                                                                               ','.join(['"{}"'.format(x) for x in r_obj.keys()]))

                            
    else: # Python
        
        for index, cell in enumerate(nb['cells']):

            if str(cell['cell_type']) == 'code': 

                try:
                    root_ast = ast.parse(str(cell['source']))
                    names = list({node.id for node in ast.walk(root_ast) if isinstance(node, ast.Name)})

                    if len(names) == 1:

                        if names[0] == 'data_path':
                            ciop.log('INFO', 'cell {} updated with \'data_path\' value {}'.format(index, tmp_dir))
                            cell['source'] = 'data_path = \'{}\''.format(tmp_dir)  

                        if names[0] == 'input_identifier':
                            ciop.log('INFO', 'cell {} updated with \'input_identifier\' value {}'.format(index, tmp_dir))
                            cell['source'] = 'input_identifier = \'{}\''.format(identifier)  

                        if names[0] == 'input_reference':
                            ciop.log('INFO', 'cell {} updated with \'input_reference\' value {}'.format(index, reference))
                            cell['source'] = 'input_reference = \'{}\''.format(reference)  

                    if len(names) != 2:
                        continue

                    if names[0] == 'dict' or names[1] == 'dict':

                        # deal with the alphabetical order
                        if names[1] == 'dict': 
                            names[1] = names[0]
                            names[0] = 'dict'

                        exec(str(cell['source'])) in globals(), locals()

                        if names[0] == 'dict' and 'title' in eval(names[1]).keys() and 'abstract' in eval(names[1]).keys() and 'id' in eval(names[1]).keys() and 'value' in eval(names[1]).keys():

                            eval(names[1])['value'] = ciop.getparam(eval(names[1])['id'])

                            new_source = 'dict(['

                            for i, keys in enumerate(eval(names[1])):
                                if i == 0: 
                                    new_source = new_source + '( "{}", "{}")' % (keys, eval(names[1])[keys]) 
                                else:
                                    new_source = new_source + ',( "{}", "{}")' % (keys, eval(names[1])[keys]) 

                            new_source = new_source + '])'

                            cell['source'] = '{} = {}'.format(names[1], new_source)

                            ciop.log('INFO', 'cell {} {} updated'.format(index, names[1]))

                except SyntaxError:

                    continue


    
 
def reproducibility(path, reference):
    
    ciop.log('INFO', 'Create stage-in notebook for reproducibility')
    
    global tmp_dir
        
    nb_stagein = nbf.v4.new_notebook()
    code = []
      
    code.append(nbf.v4.new_code_cell("""\
import os
import sys
sys.path.append('/opt/anaconda/bin/')
import cioppy
ciop = cioppy.Cioppy()"""))
    
    code.append(nbf.v4.new_code_cell('tmp_dir = "' + tmp_dir + '"'))
    code.append(nbf.v4.new_code_cell('os.makedirs(tmp_dir)'))
    
    #for reference in references:
    code.append(nbf.v4.new_code_cell('reference = "' + reference + '"'))
    code.append(nbf.v4.new_code_cell("search = ciop.search(end_point = reference, params = [], output_fields='enclosure,identifier', model='GeoTime')"))
    code.append(nbf.v4.new_code_cell("retrieved = ciop.copy(search[0]['enclosure'], tmp_dir)"))
    code.append(nbf.v4.new_code_cell("assert(retrieved)"))
        
    nb_stagein['cells'] = code
    fname = os.path.join(path, 'stage-in.ipynb')
    with open(fname, 'w') as f:
        nbf.write(nb_stagein, f)
    
    #ciop.publish('stage-in.ipynb', metalink=True)
    
def stage_in(reference):
 
    ciop.log('INFO', 'The input reference is: {}'.format(reference))    
      
    search = ciop.search(end_point = reference, params = [], output_fields='enclosure,identifier', model='GeoTime')
    assert(search), sys.exit(ERR_RESOLUTION)
  
    ciop.log('INFO', 'Retrieve {} from {}'.format(search[0]['identifier'], search[0]['enclosure']))
    retrieved = ciop.copy(search[0]['enclosure'], tmp_dir)
    assert(retrieved), sys.exit(ERR_STAGEIN)

    identifiers.append(search[0]['identifier'])
    enclosures.append(search[0]['enclosure'])
    local_files.append(retrieved)    
  

def execute(identifier, reference, nb_source, nb_target, kernel = 'python2'):
    
    global nb
   
    nb = nbf.read(nb_source, 4)
    
    ciop.log('INFO', 'Execute notebook')
    
    parametrize(identifier, reference)
    
    # execute the notebook
    ep = ExecutePreprocessor(timeout=50000, kernel_name=kernel)

    try:
        out = ep.preprocess(nb, {'metadata': {'path': './'}})
    except CellExecutionError:
        out = None
        ciop.log('ERROR', 'Error executing the notebook "{}".'.format(nb_source))

        with io.open(nb_target, 'wt') as file:
            nbf.write(nb, file)

      
        ciop.publish(nb_target, metalink=True)
        raise
    finally:
        if out is not None:
            for cell in out[0]['cells']:
                if 'outputs' in cell.keys():
                    for output in cell['outputs']:
                        if output['output_type'] == 'stream' and output['name'] == 'stderr':
                            sys.stderr.write(output['text'])


        ciop.log('INFO', 'Write notebook')  
        with io.open(nb_target, 'wt') as file:
            nbf.write(nb, file)


def publish(runtime):
   
    # publish
    ciop.log('INFO', 'Publishing ') 
    ciop.publish(runtime, metalink=True, recursive=True) 

def clean_up(runtime):
           
    # clean-up 
    shutil.rmtree(runtime)
    # delete last retrieved file
    shutil.rmtree(local_files[-1])
        
def main():

    # create the folder for the data stage-in
    global tmp_dir
    tmp_dir = os.path.join('/tmp', 'workspace-' + str(uuid.uuid4()), 'data') 
    os.makedirs(tmp_dir)

    os.environ['IPYTHONDIR'] = tmp_dir

    # Loops over all the inputs
    for reference in sys.stdin:
        references.append(reference.rstrip())
    
    for reference in references:
               
        stage_in(reference)

        runtime = os.path.join(ciop.tmp_dir, str(uuid.uuid4()))    

        os.makedirs(runtime)
        os.chdir(runtime)

        # create the reproducibility notebook for data stage-in
        reproducibility(runtime, reference)

        # execute the notebook
        nb_source = os.path.join('/application', 'notebook', 'libexec', 'input.ipynb')
        nb_target = os.path.join(runtime, 'result.ipynb')  
        
        {% if cookiecutter.python == "R" %}
        execute(identifiers[-1], reference, nb_source, nb_target, 'ir') 
        {% else %}
        execute(identifiers[-1], reference, nb_source, nb_target, '{{cookiecutter.environment}}')  
        {% endif %}
        
        # publish
        publish(runtime)   

        # clean-up 
        clean_up(runtime)

    # clean-up workspace folder
    shutil.rmtree(os.path.split(tmp_dir)[0])
try:
    main()
except SystemExit as e:
    if e.args[0]:
        clean_exit(e.args[0])
    raise
else:
    atexit.register(clean_exit, 0)


