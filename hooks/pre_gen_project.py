import sys

artifact_id = '{{cookiecutter.artifactId}}'
    
if artifact_id.upper() == artifact_id:
    
    print('ERROR: {0} is not a valid artifact id, lower case only'.format(artifact_id))

    # exits with status 1 to indicate failure
    sys.exit(1)