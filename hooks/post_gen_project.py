import os
import subprocess
import shutil 

def launch_process(args):
 
    process = subprocess.Popen(args,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT)
   

    stdout = process.communicate()[0]

    if process.returncode != 0 :

        message='Error executing {} ({})'.format(' '.join(args), process.returncode)
    
    else: 
    
        message='Execution completed ({})'.format(process.returncode)

    return message, stdout



    
    
def main():

    
    {% if cookiecutter.python == "R" %}
    ### R ###
    shutil.move('src/main/app-resources/notebook/libexec/input.R.ipynb',
                'src/main/app-resources/notebook/libexec/input.ipynb')
    
    os.remove('src/main/app-resources/notebook/libexec/input.python.ipynb')
    
    shutil.rmtree('src/main/app-resources/dependencies/python')
    
    steps = ['Updating conda...',
        'Creating the {{cookiecutter.environment}} conda environment for R...',
        'Configuring a Jupyter kernel for the {{cookiecutter.environment}} conda environment']

    
    commands = ['sudo conda update conda -y', 
                'sudo conda env create --file=src/main/app-resources/dependencies/R/environment.yml',
                'sudo /opt/anaconda/envs/{{cookiecutter.environment}}/bin/python -m ipykernel install --name {{cookiecutter.environment}}']


    for index, command in enumerate(commands):
        print (steps[index])
        launch_process(command.split(' '))

    print ('Install new Python/R modules with:\n')
    print ('sudo conda install -y -n {{cookiecutter.environment}} python=3.5 <module 1> <module 2>\n')

    print ('Activate the conda environment with:\n')
    print ('conda activate {{cookiecutter.environment}}\n')
    
    {% else %}

    ### Python ### 
    
    shutil.move('src/main/app-resources/notebook/libexec/input.python.ipynb',
                'src/main/app-resources/notebook/libexec/input.ipynb')
    
    os.remove('src/main/app-resources/notebook/libexec/input.R.ipynb')
    
    shutil.rmtree('src/main/app-resources/dependencies/R')
    
    
    steps = ['Updating conda...',
	     'Installing app_descriptor_generator',
             'Creating the {{cookiecutter.environment}} conda environment with Python {{cookiecutter.python}}...',
             'Configuring a Jupyter kernel for the {{cookiecutter.environment}} conda environment']


    commands = ['sudo conda update conda -y',
		'sudo conda install app_descriptor_generator -y',
                'sudo conda env create --file=src/main/app-resources/dependencies/python/environment.yml',
                'sudo /opt/anaconda/envs/{{cookiecutter.environment}}/bin/python -m ipykernel install --name {{cookiecutter.environment}}']
    
    for index, command in enumerate(commands):
        print (steps[index])
        launch_process(command.split(' '))

    print ('Install new Python modules with:\n')
    print ('sudo conda install -y -n {{cookiecutter.environment}} python={{cookiecutter.python}} <module 1> <module 2>\n')

    print ('Activate the conda environment with:\n')
    print ('conda activate {{cookiecutter.environment}}\n')
    
    {% endif %}
    
    
    ### stage-in ###
    end_point = '{{cookiecutter.data_package}}'

    
    if not end_point:

        return

    metadata = {
      "kernelspec": {
       "display_name": "{{cookiecutter.environment}}",
       "language": "python",
       "name": "{{cookiecutter.environment}}"
    }}
    
    nb_stagein = nbf.v4.new_notebook(metadata=metadata)
    code = []

    code.append(nbf.v4.new_markdown_cell('## Ellip Notebooks'))

    code.append(nbf.v4.new_code_cell("""\
import os
import sys
import cioppy
import pandas as pd"""))


    code.append(nbf.v4.new_markdown_cell('Feed from {0}'.format(end_point)))

    code.append(nbf.v4.new_code_cell("""\
end_point = '{0}'
""".format(end_point)))
    
    code.append(nbf.v4.new_code_cell("""\
ciop = cioppy.Cioppy()

search_params = dict()
search_params['do'] = 'terradue'

search = ciop.search(end_point=end_point, 
                     params=search_params,
                     output_fields='enclosure', 
                     model='GeoTime', 
                     timeout=500000)
search"""))

    
    code.append(nbf.v4.new_code_cell("""\
data_path = '/workspace/data'    
    """))
                                     
    code.append(nbf.v4.new_code_cell("""\

for index, entry in enumerate(search):
    print entry['enclosure']
    ciop.copy(entry['enclosure'], data_path)
"""))
    
    nb_stagein['cells'] = code

    notebook_path = 'src/main/app-resources/notebook/libexec'

    with open(os.path.join(notebook_path, 'stage-in.ipynb'), 'w') as f:
        nbf.write(nb_stagein, f)
    

    
    
if __name__ == '__main__':
    main()
